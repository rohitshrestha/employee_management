import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  url = 'https://reqres.in/api/users/';

  public empList: any = new BehaviorSubject([]);

  constructor(private http: HttpClient) {}

  setData(data: any): void {
    console.log(data);
    this.empList.next(data);
  }

  getData(): any {
    return this.empList.asObservable();
  }

  // getting data from API res....
  getDataApi(): any {
    return this.http.get('https://reqres.in/api/users?page=2');
  }

  // posting data in the API
  postDataApi(data: any) {
    return this.http.post(this.url, data);
  }
  updateDataApi(data: any) {
    return this.http.put(this.url + 2, data);
  }


}
