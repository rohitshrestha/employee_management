import { EmployeeService } from '../employee.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css'],
})
export class EmployeeListComponent implements OnInit {
  empList: any = [];
  empApiForm: FormGroup | any;

  constructor(
    public employeeService: EmployeeService,
    private router: Router,
    public fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.empApiForm = this.fb.group({
      name: ['', Validators.required],
      job: ['', Validators.required],
    });

    this.employeeService.getDataApi().subscribe((res: any) => {
      // debugger;
      console.log(res.data);
      this.empList = res.data;
      // this.empApiForm.patchValue
    });
  }

  deleteData(index: number): void {
    console.log(index);
    console.log(typeof this.empList);

    this.empList = this.empList.filter((x: any) => x.id != index);
  }

  updateData(id: any): void {
    // console.log(item.id);
    this.router.navigate([`/update/${id}`]);
    console.log(this.empList);
  }

  getApiFormData(): void {
    if (this.empApiForm.invalid) {
      alert('Fill All the fields');
      return;
    }
    const data = this.empApiForm.value;
    this.employeeService.postDataApi(data).subscribe(res => {
      console.log(res);
    });

    // this.empForm.reset();
    // alert('Employee Added');

    console.log(data);
  }
}
